import webpack from 'webpack'
import * as http from 'http'
import HtmlWebpackPlugin from 'html-webpack-plugin';
const port = 8082;
let config : webpack.Configuration = {
    mode: "development",
    entry: {
        app: "./index.tsx"
    },
    plugins: [
        new HtmlWebpackPlugin({

            filename: 'index.html',
            template: 'index.html'})
    ],
    devtool: "#@cheap-eval-source-map",
    devServer:{
        port: port,
        disableHostCheck: true,
        proxy: [
            {
                context: "/iportal",
                changeOrigin: true,
                pathRewrite:{
                    "^/iportal" : ""
                },
                target: "https://itest.supermapol.com",
                onProxyReq: (()=>{
                    let cookie: string = "";
                    let setcookie: string = "/setcookie/"
                    return (proxyReq: http.ClientRequest, req: http.IncomingMessage, res: http.ServerResponse): void => {
                        if(req.url && req.url?.includes(setcookie)) {
                            let index = req.url.indexOf(setcookie);
                            cookie = req.url.substring(index + setcookie.length);
                            console.log(cookie);
                            proxyReq.end();
                            res.end();
                            return;
                        }
                        proxyReq.setHeader("Cookie", cookie);
                    }
                })()
            }
        ]
    },
    module: {
        rules:[
            {
                test: /\.tsx?$/,
                loader: "ts-loader"
            }
        ]
    }
}

export default config;